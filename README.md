### Installing Scoop

- iwr -useb get.scoop.sh | iex

### Needed buckets to add to your scoop

- scoop bucket add extras
- scoop bucket add java
- scoop bucket add versions
- scoop bucket add shayvank https://www.bitbucket.org/shayvank/my-scoop

### Loading my apps

- scoop install shays-setup